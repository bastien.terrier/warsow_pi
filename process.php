<?php

    $file = "\Program Files (x86)\Warsow 2.1\basewsw\dedicated_autoexec.cfg";

    $dedicated_autoexec = file_get_contents($file);

    
    //g_gametype
    if(isset($_POST["g_gametype"])){
        $dedicated_autoexec = preg_replace("/set g_gametype \".*\"/i", "set g_gametype \"" . $_POST["g_gametype"] . "\"", $dedicated_autoexec);
    }

    //g_instagib
    if(isset($_POST["g_instagib"])){
        $dedicated_autoexec = preg_replace("/set g_instagib \".*\"/i", "set g_instagib \"1\"", $dedicated_autoexec);
    }
    else{
        $dedicated_autoexec = preg_replace("/set g_instagib \".*\"/i", "set g_instagib \"0\"", $dedicated_autoexec);
    }

    //g_instajump
    if(isset($_POST["g_instajump"])){
        $dedicated_autoexec = preg_replace("/set g_instajump \".*\"/i", "set g_instajump \"1\"", $dedicated_autoexec);
    }
    else{
        $dedicated_autoexec = preg_replace("/set g_instajump \".*\"/i", "set g_instajump \"0\"", $dedicated_autoexec);
    }

    //g_instashield
    if(isset($_POST["g_instashield"])){
        $dedicated_autoexec = preg_replace("/set g_instashield \".*\"/i", "set g_instashield \"1\"", $dedicated_autoexec);
    }
    else{
        $dedicated_autoexec = preg_replace("/set g_instashield \".*\"/i", "set g_instashield \"0\"", $dedicated_autoexec);
    }

    //sv_defaultmap
    if(isset($_POST["sv_defaultmap"])){
        $dedicated_autoexec = preg_replace("/set sv_defaultmap \".*\"/i", "set sv_defaultmap \"" . $_POST["sv_defaultmap"] . "\"", $dedicated_autoexec);
    }

    //g_maplist
    if(isset($_POST['g_maplist'])){
        $s_maplist = "";
        foreach ($_POST['g_maplist'] as $map){
            $s_maplist = $s_maplist . " " . $map;
        }
        $dedicated_autoexec = preg_replace("/set g_maplist \".*\"/i", "set g_maplist \"" . $s_maplist . "\"", $dedicated_autoexec);
    }
    
    //g_maprotation
    if(isset($_POST["g_maprotation"])){
        $dedicated_autoexec = preg_replace("/set g_maprotation .*/i", "set g_maprotation " . $_POST["g_maprotation"], $dedicated_autoexec);
    }

    //Ecriture dans le fichier
    if(file_put_contents($file, $dedicated_autoexec)){
        echo "Modification du fichier réussie ! ";        

        //Reset le server
        exec("Taskkill /IM wsw_server_x64.exe /F");

    }
    else{
        echo "Echec !";
    }


?>