<?php

$permitted_file = array("ca", "bomb", "ctf", "ctftactics", "da", "dm", "duel", "ffa", "headhunt", "race", "tdm");


    if(isset($_POST["conf"])){


        if(in_array($_POST["conf"], $permitted_file)){

            $file = "\Program Files (x86)\Warsow 2.1\basewsw\configs\server\gametypes\\" . $_POST["conf"] . ".cfg";

            $all_file = file_get_contents($file);

            ?>

            <form method="post" action="displayConfig.php" id="configFormToSend">

                <div class="form-group">
                    <label class="form-label">Contenu du fichier <b><?php echo $_POST["conf"] ;?>.cfg</b></label>
                    <textarea class="form-input" name="conf-file-content" id="conf-file-content" rows="20"><?php echo $all_file; ?></textarea>
                </div>

                <input type="hidden" id="sendConfigForm" name="sendConfigForm" value="1" />
                <input type="hidden" id="ConfigMode" name="ConfigMode" value="<?php echo $_POST["conf"] ;?>" />


                <button class="btn btn-success" id="confirm-config-file">Mettre à jour le fichier</button>
            
            </form>

            <?php
        }
        else{
            echo "Impossible d'accéder au fichier demandé !";
        }
        
    }
    else if(isset($_POST["sendConfigForm"]) && $_POST["sendConfigForm"] == 1 && isset($_POST["conf-file-content"]) && $_POST["conf-file-content"] != "" && isset($_POST["ConfigMode"]) && $_POST["ConfigMode"] != ""){

        if(in_array($_POST["ConfigMode"], $permitted_file)){

            $file = "\Program Files (x86)\Warsow 2.1\basewsw\configs\server\gametypes\\" . $_POST["ConfigMode"] . ".cfg";
            if(file_put_contents($file, $_POST["conf-file-content"]))
                echo "<i>Modification du fichier réussie !<i>";
            else
                echo "<b>Erreur lors de la modification ! </b>";            
        }
        else{
            echo "Impossible d'accéder au fichier demandé !";
        }

    }
?>