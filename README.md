# Params to configure

## Gamemode
- [ ] g_gametype (+++)
- [ ] g_instagib (+++)
- [ ] g_instajump (+)
- [ ] g_instashield (-)

## Maps
- [ ] sv_defaultmap (+++)
- [ ] g_maplist (+++)
- [ ] g_maprotation (+)

## Others
- [ ] g_scorelimit (++)
- [ ] g_timelimit (++)
- [ ] g_warmup_enabled (+)
- [ ] g_warmup_timelimit (+)
- [ ] g_countdown_time (+)
- [ ] g_numbots (-)