$(function(){

    $(".close-modal").click(function(){
        $("#modal-infos").removeClass("active");
    })

    function displayInfos(str){
        $("#modal-infos").addClass("active");
        $("#modal-infos .modal-body .content").html(str);
    }

    function displayConfig(gametype){
        $("#server-config-content").html(gametype);
    }

    /**
     * GAMEMODE
     */
    $("#g_gametype").on("change", function(){

        $("#server-config-title").html("Configuration du mode <b>" + $(this).val() + "</b>");
        $.post('displayConfig.php', {conf: $(this).val()}, function(data){
            displayConfig(data);
        });
    });

    $("#g_gametype").trigger("change");

    $("#g_instagib").on("change", function(){
        //logPreview("g_instagib : " + convertBoolToInt(this.checked));
    });

    $("#g_instajump").on("change", function(){
        //logPreview("g_instajump : " + convertBoolToInt(this.checked));
    });

    $("#g_instashield").on("change", function(){
        //logPreview("g_instashield : " + convertBoolToInt(this.checked));
    });

    /**
     * MAPS
     */

    $("#sv_defaultmap").on("change", function(){
        //logPreview("sv_defaultmap : " +  $(this).val());
    });

    $("#g_maplist").on("change", function(){
        //logPreview("g_maplist : " +  $(this).val());
    });

    $("#g_maprotation").on("change", function(){
        //logPreview("g_maprotation : " +  $(this).val());
    });
    


    $("#sendForm").on("click", function(e){

        e.preventDefault();

        $.post('process.php', $('#theForm').serialize(), function(data){
            displayInfos(data);
        });


    });

    /**
     * CONFIGURATION GAMETYPE
     */
    $(document).on("click", "#confirm-config-file", function(e){
        
        e.preventDefault();

        $.post('displayConfig.php', $("#configFormToSend").serialize(), function(data){
            displayConfig(data);
        });
    })
});


