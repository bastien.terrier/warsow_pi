<!DOCTYPE html>
<html>
<head>
    <title>Warsow Server Configuration</title>
    <link rel="icon" href="favicon.ico" />
    <link rel="stylesheet" href="spectre-0.5.5/dist/spectre.min.css">
    <link rel="stylesheet" href="spectre-0.5.5/dist/spectre-exp.min.css">
    <link rel="stylesheet" href="spectre-0.5.5/dist/spectre-icons.min.css">
</head>
<body>
<div style="width:45% !important; margin-left:5%;float: left;">


<?php

    //display all maps
    function displayMaps(){
        $output = array();
        exec("dir \"C:\Program Files (x86)\Warsow 2.1\basewsw\"", $output);
        echo count($output);
        foreach ($output as $filemap){
            $explodeMap = preg_split("/[\s]+/", $filemap);
            if(isset($explodeMap[3])){
                
                if(strpos($explodeMap[3], '.pk3') !== false){
                    if(substr($explodeMap[3], 0, 5) == "map_w")
                        echo "<option>" . substr($explodeMap[3], 4, strlen($explodeMap[3]) - 8) . "</option>";
                    else if(substr($explodeMap[3], 0, 2) == "cw")
                        echo "<option>" . substr($explodeMap[3], 0, strlen($explodeMap[3]) - 4) . "</option>";
                }
            }
                
        }
    }

?>
    <h1>Configuration Générale</h1>

    <!--<form method="POST" action="process.php">-->
    <form id="theForm">

    <h2>Gamemode</h2>

        <!-- g_gametype - select control -->
        <div class="form-group">
            <select class="form-select" id="g_gametype" name="g_gametype">
                <option selected="selected">ca</option>
                <option>bomb</option>                
                <option>ctf</option>
                <option>ctftactics</option>
                <option>da</option>
                <option>dm</option>
                <option>duel</option>
                <option>ffa</option>
                <option>headhunt</option>
                <option>race</option>
                <option>tdm</option>
            </select>
        </div>

        <!-- g_instagib - switch control -->
        <div class="form-group">
            <label class="form-switch">
                <input type="checkbox" id="g_instagib" name="g_instagib">
                <i class="form-icon"></i> g_instagib
            </label>
        </div>

        <!-- g_instajump - switch control -->
        <div class="form-group">
            <label class="form-switch">
                <input type="checkbox" id="g_instajump" name="g_instajump">
                <i class="form-icon"></i> g_instajump
            </label>
        </div>

        <!-- g_instashield - switch control -->
        <div class="form-group">
            <label class="form-switch">
                <input type="checkbox" id="g_instashield" name="g_instashield">
                <i class="form-icon"></i> g_instashield
            </label>
        </div>    

    <h2>Maps</h2>

        <!-- sv_defaultmap - select control -->
        <div class="form-group">
            <label>
                    sv_defaultmap
                    <select class="form-select" id="sv_defaultmap" name="sv_defaultmap">
                        <?php
                            displayMaps();
                        ?>
                    </select>
            </label>
            
        </div>

        <!-- g_maplist - multiple select control -->
        <div class="form-group">
            <label>
                    g_maplist
                    <select class="form-select" id="g_maplist" name="g_maplist[]" multiple>
                        <?php
                            displayMaps();
                        ?>
                    </select>
            </label>
            
        </div>    

        <!-- g_maprotation - select control -->
        <div class="form-group">
            <label>
                    g_maprotation
                    <select class="form-select" id="g_maprotation" name="g_maprotation">
                        <option value="0">same map</option>
                        <option value="1">in order</option>
                        <option value="2">random</option>
                    </select>
            </label>
            
        </div>

    <button type="submit" class="btn btn-success" id="sendForm">Valider</button>

    </form>
</div>


<div id="server-config-div" style="width:45% !important; margin-left:5%;float: left;">
    <h1 id="server-config-title">XXXX</h1>

    <div id="server-config-content" style="height: 100%; overflow-y:scroll;">

    </div>
</div>

<div class="modal modal-sm" id="modal-infos"><a class="modal-overlay" href="#modals-sizes" aria-label="Close"></a>
    <div class="modal-container" role="document">
        <div class="modal-header"><a class="btn btn-clear float-right close-modal" href="#modals-sizes" aria-label="Close"></a>
        <div class="modal-title h5">Informations</div>
    </div>
    <div class="modal-body">
        <div class="content">
        </div>
    </div>
        <div class="modal-footer">
            <a class="btn btn-link close-modal" href="#modals-sizes" aria-label="Close">Close</a>
        </div>
    </div>
</div>

    <script src="js/jquery-3.3.1.min.js"></script>
    <script  src="js/process.js"></script>

</body>
</html>